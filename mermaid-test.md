It's a markdown file with a mermaid diagram.

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```
